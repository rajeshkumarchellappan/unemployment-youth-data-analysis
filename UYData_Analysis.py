#!/usr/bin/env python
# coding: utf-8

# # Unemployed Youth Data Analysis

# In this project, we try to figure out the unemployment of youth globally using the API SL dataset available on the internet.
"""
Author: H. Ibrahimy
Dataset:Unemployed Youth Globally privided by API SL 2020
Date of Analysis: Nov. 2020
"""
# In[2]:


import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import style
import numpy as np
style.use("fivethirtyeight")


# The file contains data from 1960 to 2020 but only data is available from 1991, so we need to delete the unwanted columns that has no data. 

# In[4]:


df = pd.read_csv("./API_SL/API_SL.csv",error_bad_lines=False ,index_col=0)


# In[7]:


df = df.set_index(['Country Code'])


# In[ ]:




